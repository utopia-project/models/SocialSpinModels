# .. Setup Sphinx .............................................................
# Install the utilities needed for sphinx ...
execute_process(
    COMMAND
        ${UTOPIA_ENV_PIP} install -r ${CMAKE_CURRENT_SOURCE_DIR}/requirements.txt
    RESULT_VARIABLE RETURN_VALUE
    OUTPUT_VARIABLE PIP_OUTPUT
    ERROR_VARIABLE PIP_OUTPUT
)
if (NOT RETURN_VALUE EQUAL "0")
    message(SEND_ERROR "Error installing doc utilities into virtualenv: "
                       "${PIP_OUTPUT}")
endif ()

# Register subdirectories
add_subdirectory("doxygen")

# Copy README to this directory; can add other files here as well
file(
    COPY
        "${CMAKE_SOURCE_DIR}/README.md"
    DESTINATION ${CMAKE_CURRENT_SOURCE_DIR}
)

# Configure sphinx and register associated files
configure_file(conf.py.in conf.py)
add_custom_target(sphinx_html
    COMMAND ${SPHINX_EXECUTABLE}
            -T -b html
            -W --keep-going -w sphinx_warnings.log
            -c ${CMAKE_CURRENT_BINARY_DIR} # conf.py dir
            -d ${CMAKE_CURRENT_BINARY_DIR}/_doctrees # doctree pickles dir
            ${CMAKE_CURRENT_SOURCE_DIR} # input dir
            ${CMAKE_CURRENT_BINARY_DIR}/html # output dir
)

# .. Targets ..................................................................

# Add the doxygen-dependent documentation target only when doxygen was found
if (TARGET doxygen_utopia_models)
    add_dependencies(sphinx_html doxygen_utopia_models)
endif ()

add_custom_target(doc)
add_dependencies(doc sphinx_html)
