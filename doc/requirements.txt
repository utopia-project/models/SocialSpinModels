Sphinx >= 4.5

sphinx-argparse
sphinx-togglebutton
breathe >= 4.13

sphinx-book-theme
recommonmark
