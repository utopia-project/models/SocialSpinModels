"""Tests of the output of the 'SocialSpinModel' model"""

import numpy as np

import pytest

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("SocialSpinModel", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# Define fixtures


# Tests -----------------------------------------------------------------------

def test_that_it_runs():
    """Tests that the model runs through with the default settings"""
    # Create a Multiverse using the default model configuration
    mv = mtc.create_mv()

    # Run a single simulation
    mv.run_single()

    # Load data using the DataManager and the default load configuration
    mv.dm.load_from_cfg(print_tree=True)
    # The `print_tree` flag creates output of which data was loaded


@pytest.mark.skip("config sets run for too long")  # TODO add custom test sets!
def test_run_and_eval_cfgs():
    """Carries out all additional configurations that were specified alongside
    the default model configuration.

    This is done automatically for all run and eval configuration pairs that
    are located in subdirectories of the ``cfgs`` directory (at the same level
    as the default model configuration).
    If no run or eval configurations are given in the subdirectories, the
    respective defaults are used.

    See :py:meth:`~utopya.model.Model.run_and_eval_cfg_paths` for more info.
    """
    # Only a very restricted number of config sets makes sense to test here,
    # the others are way too large ...
    CFG_SETS_TO_TEST = (
        "complete_graph_optimization",
        # "stable_state_repetitions",
    )

    for cfg_name, cfg_paths in mtc.default_config_sets.items():
        if cfg_name not in CFG_SETS_TO_TEST:
            print(f"\nNOT testing '{cfg_name}' config set.")
            continue
        print(f"\nTesting '{cfg_name}' config set ...")

        mv, _ = mtc.create_run_load(
            from_cfg=cfg_paths.get("run"),
            parameter_space=dict(
                num_steps=100,
                write_every=10,
                SocialSpinModels=dict(
                    graph=dict(
                        num_vertices=100,
                    ),
                ),
            ),
        )
        mv.pm.plot_from_cfg(plots_cfg=cfg_paths.get("eval"))

        print(f"Succeeded running and evaluating '{cfg_name}'.\n")
