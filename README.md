# Social Spin Models

This repository implements a computer model of collective decision-making using the [Utopia modelling framework][Utopia].

Specifically, it implements the `SocialSpinModel`: an agent-based network model with heterogeneously distributed biases and locally non-linear social interactions.
The `SocialSpinModel` builds on a model proposed by [Hartnett *et al.*](https://doi.org/10.1103/PhysRevLett.116.038701) and extends it by introducing an additional noise process and arbitrary network topologies.

So far, the following publications resulted from studies performed with this model:

- [*Collective decision-making with heterogeneous biases: Role of network topology and susceptibility*](https://doi.org/10.48550/arXiv.2411.19829)  
  Yunus Sevinchan, Petro Sarkanych, Abi Tenenbaum, Yurij Holovatch, Pawel Romanczuk  
  *arXiv preprint* (submitted to Physical Review Research) · **2025**

- [*Consensus decision making on a complete graph: complex behaviour from simple assumptions*](https://doi.org/10.5488/cmp.27.33801)  
  Petro Sarkanych, Yunus Sevinchan, Mariana Krasnytska, Pawel Romanczuk and Yurij Holovatch  
  *Condensed Matter Physics* · **2024**

The following works used a different but equivalent implementation:

- [*Individual bias and fluctuations in collective decision making: from algorithms to Hamiltonians*](https://doi.org/10.1088/1478-3975/acd6ce)  
  Petro Sarkanych, Mariana Krasnytska, Luis Gómez-Nava, Pawel Romanczuk and Yurij Holovatch  
  *Physical Biology* · **2023**


#### Table of Contents
* [Installation](#installation)
* [Model Documentation](#model-documentation)
* [Quickstart](#quickstart)
* [Information for Developers](#information-for-developers)
* [Dependencies](#dependencies)

---

## Installation
**Note:** If not mentioned explicitly, all instructions and considerations from the [main repository][Utopia] still apply here. ☝️

The following instructions assume that you built Utopia in a development environment, as indicated in the framework repository's [README](https://gitlab.com/utopia-project/utopia#utopia).

### Step-by-step Instructions
These instructions are intended for **Ubuntu**-like or **macOS** setups.
Since **Windows** supports the installation of Ubuntu via [Windows Subsystem for Linux](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux), Utopia and this model can also be installed on Windows.
Follow the [WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install Ubuntu, then follow the instructions for Ubuntu in this README.



#### 0 — Setup Utopia
Follow the setup procedure of the Utopia framework repository as described in its [README](https://gitlab.com/utopia-project/utopia#utopia).

In the Utopia framework repository, make sure to export the package registry by calling:

```bash
cmake -DCMAKE_EXPORT_PACKAGE_REGISTRY=On ..
```


#### 1 — Clone *this* repository
Enter the `Utopia` directory into which you previously cloned the main repository.

Then clone the repository to the `Utopia` directory by using the following command:

```bash
git clone --recurse-submodules <CLONE-URL>
```

Inside your top level `Utopia` directory, there should now be (at least) two repositories:
* `utopia`: the framework repository
* `SocialSpinModels`: *this* repository


#### 2 — Install dependencies
This repository does not require _additional_ dependencies beyond [those of the Utopia framework][Utopia_deps].

Continue to the next step. 🙃


#### 3 — Configure and build
In your cloned `SocialSpinModels` repository, create a build directory:

```bash
cd SocialSpinModels
mkdir build
```

Enter the build directory and invoke CMake:

```bash
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
```

The terminal output will show the configuration steps, which includes the installation of further Python dependencies and registration of the models with the Utopia frontend.

*Note:* The `Release` build type enables compiler optimizations, thus significantly speeding up the actual simulations.

After this, you can build a specific model or all models implemented in this repository using:

```bash
make SocialSpinModel
make -j4 all    # builds all models, using 4 CPUs
```


#### 4 — Run a model :tada:
You should now be able to run model simulations via Utopia's frontend, [utopya][utopya].

The only requirement is that you have activated Utopia's virtual environment, `utopia-env`:
You can do so by navigating to the `build` directory and calling

```bash
source ./activate
```

You can now use the [`utopia` command line interface][utopya-cli] to run a model:

```bash
utopia run SocialSpinModel
```

The log output will show the progress of the simulation and data evaluation.
By default, the output will be stored in `~/utopya_output/<model_name>`, using the timestamp of the start of the simulation.

With pre-configured configuration sets, certain scenarios can be easily reproduced:

```bash
utopia run SocialSpinModel --cs WSG__weakly_biased
```

This will run the configuration defined in `src/models/SocialSpinModel/cfgs/WSG__weakly_biased` (a Watts-Strogatz-rewired 2D lattice graph with a weakly biased agent scenario) and will create the corresponding plots.


### Troubleshooting
* If your model requires a certain branch of the Utopia framework, make sure to re-configure and re-build the framework repository after switching the branch.

* If the `cmake ..` command fails because it cannot locate the Utopia main
    repository, something went wrong with the CMake User Package Registry.
    You can always specify the location of the package's _build_ directory via
    the CMake variable `Utopia_DIR` and circumvent this CMake feature:

    ```bash
    cmake -DUtopia_DIR=<path/to/>utopia/build ..
    ```

  Alternatively, you can try to reconfigure the Utopia *framework* repository with

    ```bash
    cmake -DCMAKE_EXPORT_PACKAGE_REGISTRY=On ..
    ```




## Model Documentation
Unlike the [main Utopia documentation](https://docs.utopia-project.org/), the models included in this repository come with their own documentation which has to be built locally.
It is *not* available online.

To build these docs locally, navigate to the `build` directory and execute

```bash
make doc
```

The [Sphinx][sphinx_doc]-built user documentation will then be located at `build/doc/html/`, and the C++ [doxygen][doxygen]-documentation can be found at `build/doc/doxygen/html/`.
Open the respective `index.html` files to browse the documentation.



## Quickstart
### How to run a model?
The Utopia command line interface (CLI) is, by default, only available in a Python virtual environment, in which [`utopya` (the Utopia frontend)][utopya] and its dependencies are installed.
This virtual environment is located in the **main** (`utopia`) repository's build directory.
However, symlinks to the `activate` and `run-in-utopia-env` scripts are provided within the build directory of this project.
You can enter it by specifying the correct path:

```bash
source <path/to/{utopia,models}>/build/activate
```

Now, your shell should be prefixed with `(utopia-env)`.
All the following should take place inside this virtual environment.

As you have already done with the `dummy` model, the basic command to run a model named `SocialSpinModel` is:

```bash
utopia run SocialSpinModel
```

You can list all models registered with the frontend by calling:

```bash
utopia model ls
```



## Information for Developers
### New to Utopia? How do I implement a model?
Please refer to the [documentation of the framework repository](https://docs.utopia-project.org/).


### Testing
Not all test groups of the main project are available in this repository.

| Identifier        | Test description   |
| ----------------- | ------------------ |
| `model_<name>`    | The C++ model tests of model with name `<name>` |
| `models`†         | The C++ and Python tests for _all_ models |
| `models_python`†‡ | All python model tests (from `python/model_tests`) |
| `all`             | All of the above. (Go make yourself a hot beverage, when invoking this.) |

_Note:_
* Identifiers marked with `†` require all models to be built (by running `make all`).
* Identifiers marked with `‡` do _not_ have a corresponding `build_tests_*` target.
* Notice that you cannot execute tests for models of the main project in this repository.


#### Evaluating Test Code Coverage
Code coverage is useful information when writing and evaluating tests.
The coverage percentage of the C++ code is reported via the GitLab CI pipeline.
Check the [`README.md` in the main repository](https://gitlab.com/utopia-project/utopia#c-code-coverage) for information on how to compile your code with coverage report flags and how to retrieve the coverage information.


## Dependencies
| Software                  | Version  | Comments |
| ------------------------- | -------- | -------- |
| [doxygen] _(optional)_    |          | for building the documentation |

These dependencies come on top of [Utopia's dependencies][Utopia_deps].
For some dependencies, a more recent version number is required than by the Utopia framework; the numbers given here take precedence and are enforced.
Note that the homebrew-based CI/CD testing image might use more recent versions, so it's actually best to have those installed.


### Additional Python Dependencies
If your models require additional Python packages (e.g., for plotting and testing), they can be added to the `python/requirements.txt` file.
All packages listed there will be automatically installed into the Python virtual environment as part of the CMake configuration.

Note that if such an installation fails, it will _not_ lead to a failing CMake configuration.


[Utopia]: https://gitlab.com/utopia-project/utopia
[Utopia_deps]: https://gitlab.com/utopia-project/utopia#dependencies
[utopya]: https://gitlab.com/utopia-project/utopya
[utopya-cli]: https://utopya.readthedocs.io/en/latest/cli/index.html

[doxygen]: http://www.doxygen.nl
[sphinx_doc]: https://www.sphinx-doc.org
