#ifndef UTOPIA_MODELS_SOCIALSPINMODEL_GRAPH_HH
#define UTOPIA_MODELS_SOCIALSPINMODEL_GRAPH_HH

#include <boost/graph/adjacency_list.hpp>

#include <utopia/core/graph.hh>

#include "entities.hh"


namespace Utopia::Models::SocialSpinModel {

/// The traits of a vertex are just the traits of a graph entity
using VertexTraits = Utopia::GraphEntityTraits<AgentState>;

/// A vertex is a graph entity with vertex traits
using Vertex = GraphEntity<VertexTraits>;

/// The vertex container type
using VertexContainer = boost::vecS;

/// The edge container type
using EdgeContainer = boost::vecS;

/// The graph type
using Graph = boost::adjacency_list<
    EdgeContainer,
    VertexContainer,
    boost::undirectedS,
    Vertex
>;


} // namespace

#endif // UTOPIA_MODELS_SOCIALSPINMODEL_GRAPH_HH
