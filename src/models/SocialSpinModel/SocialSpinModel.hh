#ifndef UTOPIA_MODELS_SOCIALSPINMODEL_HH
#define UTOPIA_MODELS_SOCIALSPINMODEL_HH

// standard library includes
#include <limits>
#include <stdexcept>
#include <cstdint>
#include <iterator>
#include <random>
#include <functional>
#include <type_traits>
#include <vector>

// third-party library includes
#include <spdlog/spdlog.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

// Utopia-related includes
#include <tuple>
#include <utopia/core/graph/apply.hh>
#include <utopia/core/graph/iterator.hh>
#include <utopia/core/model.hh>
#include <utopia/core/apply.hh>
#include <utopia/core/types.hh>
#include <utopia/core/graph.hh>
#include <utopia/data_io/graph_utils.hh>

#include <UtopiaUtils/utils/rng.hh>
#include <UtopiaUtils/utils/iterators.hh>

// overload of graph apply_rule that re-uses a cache container
// TODO Remove once utopia!304 is merged
//      https://gitlab.com/utopia-project/utopia/-/merge_requests/304
#include "../../utils/graph/apply.hh"
#include "../../utils/graph/creation.hh"
#include "../../utils/graph/stats.hh"
#include "../../utils/iterator.hh"

// model-related includes
#include "entities.hh"
#include "graph.hh"


namespace Utopia::Models::SocialSpinModel {

// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Use a custom (faster) RNG
using ModelRNG = Utopia::Utils::DefaultRNG;

/// Type helper to define types used by the model
using ModelTypes = Utopia::ModelTypes<ModelRNG>;



// ++ Model definition ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// The SocialSpinModel Model
template<typename GraphType=Graph>
class SocialSpinModel:
    public Model<SocialSpinModel<GraphType>, ModelTypes>
{
public:
    /// The type of the Model base class of this derived class
    using Base = Model<SocialSpinModel, ModelTypes>;

    /// Data type of the group to write model data to, holding datasets
    using DataGroup = typename Base::DataGroup;

    /// Data type for a dataset
    using DataSet = typename Base::DataSet;

    // .. Graph-related types and rule types ..................................
    /// The traits of the graph
    using GraphTraits = boost::graph_traits<GraphType>;

    /// Type for a vertex descriptor
    using VertexDesc =
        typename boost::graph_traits<GraphType>::vertex_descriptor;

    /// Rule function operating on vertices returning void
    using VertexVoidRule =
        typename std::function<void(VertexDesc, GraphType&)>;

    /// Rule function operating on vertices returning a new agent state
    using VertexRule =
        typename std::function<AgentState(VertexDesc, GraphType&)>;


private:
    // Base members: _time, _name, _cfg, _hdfgrp, _rng, _monitor, _space

    // -- Members -------------------------------------------------------------
    /// If true, will compute a global mean field for social interaction
    /** This avoids repetitive looping over all agents.
     */
    bool _complete_graph_optimization;

    /// The graph
    GraphType _g;

    /// Number of nodes
    const std::size_t _N;

    // .. Graph properties ....................................................

    /// Connected component mapping, indices corresponding to nodes
    const std::vector<std::size_t> _connected_components;

    /// Connected component sizes
    /** Can also be empty, denoting that it was not computed.
     */
    const std::vector<std::size_t> _connected_component_sizes;

    /// Number of connected components
    /** Can also be zero, denoting that it was not computed.
     */
    const std::size_t _num_cc;

    /// Largest connected component
    const std::size_t _largest_connected_component;

    // .. Parameters ..........................................................

    /// Noise mode: `flip` or `random_state`
    const std::string _noise_mode;

    /// Random flipping probability (∈ [0, 1]) used in `flip` noise mode
    const double _p_flip_random;

    /// Probability of a noise event (∈ [0, 1]) used in `random_state` mode
    const double _p_noise;

    /// Steepness of social field sigmoidal
    const double _beta;

    /// External field value, re-evaluated at each time step
    double _external_field;

    // .. Helper objects ......................................................
    /// The interaction function to apply to all
    /** This is set dynamically during model initialization, depending on e.g.
     *  the selected noise mode.
     */
    VertexRule _interaction_func;

    /// The evaluated interaction sigmoid
    std::function<double(const double)> _ia_sigmoid;

    /// The external field function (over time)
    std::function<double(const std::size_t)> _external_field_function;

    /// A re-usable uniform distribution
    std::uniform_real_distribution<double> _uniform_prob_distr;


    // .. Trackers ............................................................

    /// The number of agents in up-state; used to avoid repeated iteration
    std::size_t _num_up;

    /// The number of agents in up-state; used in complete-graph optimization
    /** This is a separate value to allow it to be explicitly computed rather
     *  than implicitly assuming that ``_num_up`` is correct.
     */
    std::size_t _cgo_num_up;

    /// The history of the mean state
    /** Note that this stores the _full_ history for each repetition.
     *  Given that repetitions are typically shorter than 1000 iteration steps,
     *  this does not contribute a significant memory load.
     */
    std::vector<double> _mean_state_history;

    /// The history of the mean state for each component
    /** Like `_mean_state_history`, but resolved for each component.
     */
    std::vector<std::vector<double>> _cc_mean_state_history;

    // .. Temporary objects ...................................................

    /// The state cache (to avoid re-allocation)
    /** This container is used to temporarily store the _new_ state of each
     *  agent, before it is applied. When using a synchronous update rule,
     *  such a container is needed; re-allocating it each time would be very
     *  costly for big systems.
     */
    std::vector<AgentState> _state_cache;

    // .. Repetition management ...............................................
    /// Whether to do repetitions
    const bool _repetitions_enabled;

    /// Repetition interval, i.e. the duration of a single repetition
    /** After every such interval, the system is re-initialized, while the
     *  graph topology stays the same.
     */
    const std::size_t _repetition_interval;

    /// For convenience, the number of resets (counting the one at time zero)
    const std::size_t _num_repetitions;

    /// Which repetition the system is currently in.
    std::size_t _repetition_no;

    // .. Monitoring and Data Writing .........................................
    /// How many iteration steps of the mean state history to look at
    /** Note that this does not control how much of the history is retained;
     *  but the size of that container should be negligible anyway.
     */
    std::size_t _fluc_num_samples;

    /// Minimum mean state history samples needed for fluctuation calculation
    /** This only pertains to the monitoring of the fluctuations. If the size
     *  is smaller than this, it will output +inf instead.
     */
    std::size_t _fluc_min_samples;

    /// Number of samples for stable state magnetization
    std::size_t _stable_state_num_samples;

    /// HDF5 compression level
    unsigned _compression;

    /// Whether to write connected-components information
    bool _write_cc;

    /// Whether to write agent-level data (the state)
    bool _write_agent_data;

    /// Whether to write the bias of each agent
    bool _write_bias;

    /// Whether to write stable state magnetization and std. dev. of it
    bool _write_stable_state_mean_and_std;

    /// Whether to write the external field time series
    bool _write_external_field;

    /// Whether to write interaction energy data
    bool _write_interaction_energy;

    /// The data group containing all graph-related data (i.e. on agent-level)
    std::shared_ptr<DataGroup> _dgrp_graph;

    /// Dataset for individual agents' states
    std::shared_ptr<DataSet> _dset_state;

    /// Dataset for individual agents' bias value (written once for each rep.)
    std::shared_ptr<DataSet> _dset_bias;

    /// Dataset for mean of all agents' states, i.e.: the magnetization
    std::shared_ptr<DataSet> _dset_mean_state;

    /// Dataset for stable state, i.e. mean magnetization of each repetition
    std::shared_ptr<DataSet> _dset_stable_state;

    /// Dataset for storing the external field at all time steps
    std::shared_ptr<DataSet> _dset_external_field;

    /// Dataset for storing the interaction energy "1" at all time steps
    std::shared_ptr<DataSet> _dset_interaction_energy_1;

    /// Dataset for storing the interaction energy "2" at all time steps
    std::shared_ptr<DataSet> _dset_interaction_energy_2;

    /// Dataset for connected-component ID of each node
    std::shared_ptr<DataSet> _dset_g_connected_component;

    /// Dataset for sizes of connected-components
    std::shared_ptr<DataSet> _dset_cc_sizes;

    /// Dataset for connected-component-specific mean state time series
    std::shared_ptr<DataSet> _dset_cc_mean_state;

    /// Dataset for stable state of the largest component (for each repetition)
    std::shared_ptr<DataSet> _dset_cc_largest_component_stable_state;

public:
    // -- Model Setup ---------------------------------------------------------

    /// Construct the SocialSpinModel model
    /** \param name             Name of this model instance; is used to extract
     *                          the configuration from the parent model and
     *                          set up a HDFGroup for this instance
     *  \param parent_model     The parent model this model instance resides in
     *  \param custom_cfg       A custom configuration to use instead of the
     *                          one extracted from the parent model using the
     *                          instance name
     */
    template<class ParentModel>
    SocialSpinModel (
        const std::string& name,
        ParentModel& parent_model,
        const DataIO::Config& custom_cfg = {}
    )
    :
        Base(name, parent_model, custom_cfg)

    ,   _complete_graph_optimization{
            get_as<bool>("complete_graph_optimization", this->_cfg, false)
        }
    ,   _g(this->setup_graph(
            this->_cfg["graph"], _complete_graph_optimization
        ))
    ,   _N(boost::num_vertices(_g))

    ,   _connected_components(
            [&]() -> std::vector<std::size_t> {
                if (get_as<bool>("write_connected_components", this->_cfg)) {
                    return this->connected_components();
                }
                this->_log->info("NOT computing connected components.");
                return {};
            }()
        )
    ,   _connected_component_sizes(
            connected_component_sizes(_connected_components)
        )
    ,   _num_cc(_connected_component_sizes.size())
    ,   _largest_connected_component(
            [&]() -> std::size_t {
                if (not _num_cc) {
                    return std::numeric_limits<std::size_t>::max();
                }

                const auto largest_cc = largest_component(
                    _connected_components, _connected_component_sizes
                );
                const auto largest_cc_size =
                    _connected_component_sizes[largest_cc];

                this->_log->info(
                    "  Size of largest connected component:  {}  "
                    "({}% of {} nodes)",
                    largest_cc_size,
                    (static_cast<double>(largest_cc_size) / _N) * 100.,
                    _N
                );
                return largest_cc;
            }()
        )

    // Get parameters
    ,   _noise_mode(get_as<std::string>("noise_mode", this->_cfg))
    ,   _p_flip_random(get_as<double>("p_flip_random", this->_cfg))
    ,   _p_noise(get_as<double>("p_noise", this->_cfg))

    ,   _beta(get_as<double>("beta", this->_cfg))

    ,   _external_field(0.)

    // Pre-evaluate functionals that are required in the dynamics
    ,   _ia_sigmoid([this]() -> std::function<double(const double)> {
            // Construct lambdas for the interaction sigmoidal.
            // Pre-evaluates as much as possible to avoid repeated computations
            this->_log->info(
                "Building interaction sigmoidal ... (beta: {:.4g})", _beta
            );
            if (_beta < 1.e-10) {
                this->_log->debug("Using small-beta approximation.");
                return [](const auto social_field){
                    return 0.5 * (1.0 + social_field);
                };
            }
            else {
                const auto tanh_beta = std::tanh(_beta);
                return [this, tanh_beta](const auto social_field){
                    return 0.5 * (
                        1.0 + std::tanh(this->_beta * social_field) / tanh_beta
                    );
                };
            }
        }())

    ,   _external_field_function(build_external_field_function())

    ,   _uniform_prob_distr(0., 1.)

    ,   _num_up(0)
    ,   _cgo_num_up(0)

    ,   _mean_state_history{}
    ,   _cc_mean_state_history{}

    ,   _state_cache{}

    // Repetition management
    ,   _repetitions_enabled{
            get_as<bool>("enabled", this->_cfg["repetitions"], false)
        }
    ,   _repetition_interval(
            get_as<std::size_t>("interval", this->_cfg["repetitions"], 1000)
        )
    ,   _num_repetitions(this->_time_max / _repetition_interval)
    ,   _repetition_no(0)

    // Monitoring-related parameters
    ,   _fluc_num_samples(
            get_as<std::size_t>(
                "num_samples_for_fluctuation_monitoring", this->_cfg
            )
        )
    ,   _fluc_min_samples(
            get_as<std::size_t>(
                "min_samples_for_fluctuation_monitoring", this->_cfg
            )
        )

    ,   _stable_state_num_samples(
            get_as<std::size_t>(
                "num_samples_for_stable_state", this->_cfg
            )
        )

    // Set up data-writing related parameters
    ,   _compression(get_as<unsigned>("compression", this->_cfg))
    ,   _write_cc(get_as<bool>("write_connected_components", this->_cfg))
    ,   _write_agent_data(get_as<bool>("write_agent_data", this->_cfg))
    ,   _write_bias(get_as<bool>("write_bias", this->_cfg))
    ,   _write_stable_state_mean_and_std(
            get_as<bool>("write_stable_state_mean_and_std", this->_cfg)
        )
    ,   _write_external_field(
            get_as<double>("amplitude", this->_cfg["external_field"]) != 0
    )
    ,   _write_interaction_energy(
            get_as<bool>("write_interaction_energy", this->_cfg, true)
        )

    // Graph group and agent-specific state dataset
    ,   _dgrp_graph(DataIO::create_graph_group(_g, this->_hdfgrp, "graph"))

    ,   _dset_state(
            this->create_vertex_dset("state")
        )

    // Bias values (once or for each repetition)
    ,   _dset_bias(
            this->_dgrp_graph->open_dataset(
                "bias",
                {_repetitions_enabled ? _num_repetitions : 1, _N},
                {},
                _compression
            )
        )

    // Mean and stable state
    ,   _dset_mean_state(
            this->create_dset("mean_state", {}, _compression)
        )
    ,   _dset_stable_state(
            this->_hdfgrp->open_dataset(
                "stable_state",
                {_repetitions_enabled ? _num_repetitions : 1, 2},
                {},
                _compression
            )
        )

    // Other observables
    ,   _dset_external_field(
            this->create_dset("external_field", {}, _compression)
        )
    ,   _dset_interaction_energy_1(
            this->create_dset("interaction_energy_1", {}, _compression)
        )
    ,   _dset_interaction_energy_2(
            this->create_dset("interaction_energy_2", {}, _compression)
        )

    // Connected-component-related datasets
    ,   _dset_g_connected_component(
            this->create_vertex_dset("connected_component")
        )
    ,   _dset_cc_sizes(
            this->_hdfgrp->open_dataset(
                "cc_sizes",
                {_num_cc},
                {},
                _compression
            )
        )
    ,   _dset_cc_mean_state(
            this->create_dset("cc_mean_state", {_num_cc}, _compression)
        )
    ,   _dset_cc_largest_component_stable_state(
            this->_hdfgrp->open_dataset(
                "cc_largest_component_stable_state",
                {_repetitions_enabled ? _num_repetitions : 1, 2},
                {},
                _compression
            )
        )
    {
        // using _par_cat = typename GraphTraits::edge_parallel_category;
        // static_assert(
        //     std::is_same<_par_cat, boost::disallow_parallel_edge_tag>()
        // );

        // In repetition mode, make sure that simulation timings match
        if (_repetitions_enabled) {
            if (this->_time_max % this->_repetition_interval != 0) {
                throw std::invalid_argument(fmt::format(
                    "Invalid combination of number of simulation steps ({}) "
                    "and repetition interval ({})! The simulation time should "
                    "be an integer multiple of the repetition interval!",
                    this->_time_max, this->_repetition_interval
                ));
            }

            this->_log->info("Repetitions are ENABLED.");
            this->_log->info("  Interval:        {}", _repetition_interval);
            this->_log->info("  #repetitions:    {}",
                             this->_time_max / _repetition_interval);
        }
        else {
            this->_log->info("Repetitions NOT enabled.");
        }

        this->_log->info(
            "Samples for computing stable state:  {}",
            _stable_state_num_samples
        );

        // Reserve memory
        _state_cache.reserve(boost::num_vertices(_g));
        _mean_state_history.reserve(this->_time_max + 1);

        // Decide on noise mode
        this->_log->info("Chosen noise mode is '{}'.", _noise_mode);
        if (_noise_mode == "flip") {
            _interaction_func = _interact_and_flip_randomly;
        }
        else if (_noise_mode == "random_state") {
            _interaction_func = _interact_and_apply_noise;
        }
        else {
            throw std::invalid_argument(fmt::format(
                "Invalid value '{}' for parameter `noise_mode`! "
                "Possible values are:  flip, random_state",
                _noise_mode
            ));
        }

        // Inform about complete graph optimization
        this->_log->info(
            "Complete graph optimization is {}.\n",
            _complete_graph_optimization ? "ENABLED" : "not enabled"
        );

        // Set up agents
        this->init_agents(_g, get_as<Config>("init", this->_cfg));
        track_mean_state_history();

        // Finish dataset setup
        this->finish_dset_setup();

        // Store the (static) graph topology or other graph info
        this->store_graph();

        this->_log->info("Model is fully set up now.\n");
    }


    // .. Setup functions .....................................................

    /// Sets up the graph and evaluates complete graph optimization
    GraphType setup_graph (
        Config cfg = {},
        bool complete_graph_optimization = false
    ) {
        if (not cfg.size()) {
            cfg = this->_cfg["graph"];
        }

        const auto model = get_as<std::string>("model", cfg);
        this->_log->info("Creating {} graph ...", model);

        GraphType g;

        if (complete_graph_optimization and complete_graph_configured(cfg)) {
            // Simply create an empty graph; we don't need the links if we can
            // assume every node to be connected to every other one.
            // This saves on memory as well as on graph creation time.
            const auto N = get_as<std::size_t>("num_vertices", cfg);
            g = GraphType(N);
            this->_log->info(
                "Created empty graph with {} vertices. (Using complete graph "
                "optimization.)", boost::num_vertices(g)
            );
        }
        else {
            // Use the graph creation overload from the SSM utils, which
            // supports some additional graph creation algorithms
            // TODO Given that graph creation takes a large amount of time for
            //      the larger graphs, consider storing the graph somewhere and
            //      then loading the graph from there (on fingerprint match)
            g = Utils::Graph::create_graph<GraphType>(cfg, *this->_rng);
            this->_log->info(
                "Created {} graph with {} vertices and {} edges.\n",
                model, boost::num_vertices(g), boost::num_edges(g)
            );

            // No complete graph optimization during runtime
            _complete_graph_optimization = false;
        }

        return g;
    }

    /// Sets the agent states according to some parameters
    /** Currently this only includes the probability to be in the `up` state,
     *  but can be expanded in the future.
     */
    void init_agents (GraphType& g, const Config& cfg) {
        const auto N = boost::num_vertices(g);
        this->_log->info("Initializing states of {:d} agents ...", N);

        // .. Initial state
        const auto frac_up = get_as<double>("frac_up", cfg);
        const int N_up = std::round(frac_up * N);
        const int N_down = N - N_up;
        this->_log->info(
            "  In `up` state: {:>5d}  (real: {:.3g}%, configured: {:.3g}%)",
            N_up, 100. * N_up / N, 100. * frac_up
        );

        auto state = std::vector<bool>();
        state.reserve(N);

        state.insert(state.begin(), N_up, true);
        state.insert(state.begin(), N_down, false);

        std::shuffle(std::begin(state), std::end(state), *this->_rng);

        // .. Initial bias
        const auto frac_unbiased = get_as<double>("frac_unbiased", cfg);
        const auto frac_biased = 1. - frac_unbiased;
        const auto frac_biased_up = get_as<double>("frac_biased_up", cfg);
        const auto frac_biased_down = 1.0 - frac_biased_up;

        const auto omega_zero = 1.0;
        const auto omega_up = get_as<double>("omega_up", cfg);
        const auto omega_down = get_as<double>("omega_down", cfg);

        const int N_biased = std::round(frac_biased * N);
        const int N_biased_up = std::round(frac_biased_up * N_biased);
        const int N_biased_down = N_biased - N_biased_up;
        const int N_unbiased = N - N_biased;

        this->_log->info(
            "  N_unbiased:    {:>5d}  (real: {:.3g}%, configured: {:.3g}%)",
            N_unbiased, 100. * N_unbiased / N, 100. * frac_unbiased
        );
        this->_log->info(
            "  N_biased:      {:>5d}  (real: {:.3g}%, configured: {:.3g}%)",
            N_biased, 100. * N_biased / N, 100. * frac_biased
        );
        this->_log->info(
            "  N_biased_up:   {:>5d}  (real: {:.3g}%, configured: {:.3g}%)",
            N_biased_up, 100. * N_biased_up / N,
            100. * frac_biased_up * frac_biased
        );
        this->_log->info(
            "  N_biased_down: {:>5d}  (real: {:.3g}%, configured: {:.3g}%)",
            N_biased_down, 100. * N_biased_down / N,
            100. * frac_biased_down * frac_biased
        );

        // Prepare and fill the bias vector
        auto bias = std::vector<double>();
        bias.reserve(N);

        bias.insert(bias.begin(), N_unbiased, omega_zero);
        bias.insert(bias.begin(), N_biased_up, omega_up);
        bias.insert(bias.begin(), N_biased_down, omega_down);

        std::shuffle(std::begin(bias), std::end(bias), *this->_rng);

        // Apply to agent states and keep track of the value
        _num_up = 0;
        auto i = 0u;
        for (const auto& v : range<IterateOver::vertices>(g)) {
            g[v].state.up = state[i];
            g[v].state.bias = bias[i];
            i++;
            _num_up += state[i];
        }
        this->_log->info("Agent states initialized.\n");
    }

    /// Returns a vector of component IDs of each vertex in the given graph
    std::vector<std::size_t> connected_components () const {
        this->_log->info(
            "Computing connected components ...",
            boost::num_vertices(_g), boost::num_edges(_g)
        );

        auto cc = std::vector<std::size_t>(boost::num_vertices(_g), 0);
        auto num_cc = 0u;

        if (not _complete_graph_optimization) {
            num_cc = boost::connected_components(_g, &cc[0]);
        }
        else {
            num_cc = 1;
        }

        this->_log->info("  Found {} connected component(s).", num_cc);

        return cc;
    }

    /// Returns sizes of the respective components
    template<class C>
    std::vector<std::size_t> connected_component_sizes (const C& cc) const {
        if (cc.empty()) {
            return {};
        }
        this->_log->debug("  Computing sizes of connected components ...");

        const std::size_t max_ccid = *std::max_element(cc.begin(), cc.end());
        auto sizes = std::vector<std::size_t>(max_ccid + 1, 0);

        for (const auto& cid : cc) {
            sizes[cid] += 1;
        }

        return sizes;
    }

    /// Returns the index of the largest component
    /** If there are multiple components with maximum size, the index will
     *  be that of the first one.
     */
    template<class C, class C2 = std::vector<std::size_t>>
    std::size_t largest_component (const C& cc, C2 sizes = {}) {
        if (sizes.empty()) {
            sizes = connected_component_sizes(cc);
        }

        auto i = 0u;
        auto largest_size = 0u;
        auto largest_component = 0u;

        for (const auto& size : sizes) {
            if (size > largest_size) {
                largest_size = size;
                largest_component = i;
            }
            i++;
        }

        this->_log->debug("  Largest component index: {}", largest_component);

        return largest_component;
    }

    /// Build the external field function
    std::function<double(const std::size_t)>
    build_external_field_function () const
    {
        auto func_params = get_as<Config>("external_field", this->_cfg);

        const auto mode = get_as<std::string>("mode", func_params);
        const auto period = get_as<std::size_t>("period", func_params);
        const double amplitude = get_as<double>("amplitude", func_params);

        if (mode == "constant" or amplitude == 0) {
            return [amplitude](auto) -> double {
                return amplitude;
            };
        }
        else if (mode == "step") {
            return [amplitude, period](auto time) -> double {
                if (time/(period/2) % 2 == 0 ) {
                    return amplitude;
                }
                else {
                    return amplitude * -1;
                }
            };
        }
        else {
            throw std::invalid_argument(
                "Invalid external field mode " + mode
                + "! Possible modes: constant, step."
            );
        }
    }

private:
    /// Creates a dataset to store vertex-specific data
    /** This is always created from the associated graph and using the graph
     *  group and specified compression level.
     */
    auto create_vertex_dset (const std::string& name) {
        auto dset = this->create_dset(
            name, _dgrp_graph, {boost::num_vertices(_g)}, _compression
        );
        dset->add_attribute("is_vertex_property", true);
        dset->add_attribute("dim_name__1", "vertex_idx");
        dset->add_attribute("coords_mode__vertex_idx", "trivial");
        return dset;
    }

    /// Finishes dataset setup by adding attributes
    void finish_dset_setup () {
        _dset_mean_state->add_attribute(
            "repetitions_enabled", _repetitions_enabled
        );
        _dset_mean_state->add_attribute(
            "repetition_interval", _repetition_interval
        );

        _dset_stable_state->add_attribute("dim_name__0", "reps");
        _dset_stable_state->add_attribute("coords_mode__reps", "trivial");

        _dset_stable_state->add_attribute("dim_name__1", "kind");
        _dset_stable_state->add_attribute("coords_mode__kind", "values");
        _dset_stable_state->add_attribute(
            "coords__kind", std::vector<std::string>{"mean", "stddev"}
        );

        _dset_stable_state->add_attribute(
            "repetition_interval", _repetition_interval
        );
        _dset_stable_state->add_attribute(
            "num_samples", _stable_state_num_samples
        );

        if (_write_bias) {
            _dset_bias->add_attribute("dim_name__0", "reps");
            _dset_bias->add_attribute("coords_mode__reps", "trivial");

            _dset_bias->add_attribute("is_vertex_property", true);
            _dset_bias->add_attribute("dim_name__1", "vertex_idx");
            _dset_bias->add_attribute("coords_mode__vertex_idx", "trivial");

            _dset_bias->add_attribute(
                "repetition_interval", _repetition_interval
            );
        }

        if (_write_cc) {
            _dset_cc_sizes->add_attribute("dim_name__0", "ccid");
            _dset_cc_sizes->add_attribute("coords_mode__ccid", "trivial");

            _dset_cc_mean_state->add_attribute("dim_name__1", "ccid");
            _dset_cc_mean_state->add_attribute("coords_mode__ccid", "trivial");

            _dset_cc_largest_component_stable_state->add_attribute(
                "dim_name__0", "reps"
            );
            _dset_cc_largest_component_stable_state->add_attribute(
                "coords_mode__reps", "trivial"
            );

            _dset_cc_largest_component_stable_state->add_attribute(
                "dim_name__1", "kind"
            );
            _dset_cc_largest_component_stable_state->add_attribute(
                "coords_mode__kind", "values"
            );
            _dset_cc_largest_component_stable_state->add_attribute(
                "coords__kind", std::vector<std::string>{"mean", "stddev"}
            );

            _dset_cc_largest_component_stable_state->add_attribute(
                "repetition_interval", _repetition_interval
            );
            _dset_cc_largest_component_stable_state->add_attribute(
                "num_samples", _stable_state_num_samples
            );
        }
    }

    /// Stores the graph and associated statistics
    void store_graph () {
        const auto& cc = _connected_components;
        const auto& cc_sizes = _connected_component_sizes;

        this->_log->info("Saving graph statistics ...");
        Utils::IO::save_graph_stats(_g, this->_hdfgrp);

        if (get_as<bool>("write_degree_distribution", this->_cfg)) {
            this->_log->info("Computing and storing degree distribution ...");
            auto dd = Utils::compute_degree_distribution(_g);
            Utils::IO::save_degree_distribution(dd, this->_hdfgrp);
        }

        if (get_as<bool>("write_topology", this->_cfg)) {
            // With complete graph optimization the graph has no edges.
            // Improving error message shown to the user
            if (_complete_graph_optimization) {
                throw std::invalid_argument(
                    "Cannot write graph topology if "
                    "complete_graph_optimization is enabled"
                );
            };

            DataIO::save_graph(_g, _dgrp_graph);

            if (_write_cc) {
                this->_log->info("Saving connected components ...");
                this->_dset_g_connected_component->write(
                    cc.begin(), cc.end(),
                    [](const auto cid){ return static_cast<uint32_t>(cid); }
                );
            }
        }

        if (_write_cc) {
            this->_log->info("Saving connected component sizes ...");
            this->_dset_cc_sizes->write(
                cc_sizes.begin(), cc_sizes.end(),
                [](const auto s){ return static_cast<uint32_t>(s); }
            );

            // Some additional information as attributes
            this->_dset_cc_sizes->add_attribute(
                "largest_component", _largest_connected_component
            );
            const auto largest_cc_size =
                _connected_component_sizes[_largest_connected_component];
            this->_dset_cc_sizes->add_attribute(
                "largest_component_size", largest_cc_size
            );
            this->_dset_cc_sizes->add_attribute(
                "largest_component_rel_size",
                static_cast<double>(largest_cc_size) / _N
            );
        }
    }

public:
    // -- Simulation Control --------------------------------------------------

    /// Iterate a single step
    /** Invokes the iteration rule on all nodes, synchronously updating their
     *  state afterwards.
     */
    void perform_step () {
        // Might have to start a new repetition
        maybe_start_new_repetition();

        // Reset tracker variable
        _num_up = 0;

        // Evaluate external field value for this time step
        _external_field = _external_field_function(this->_time);

        // With complete-graph optimization, only need to determine the mean
        // field once. This value is then used in computing the social field,
        // which is the same for all agents.
        if (_complete_graph_optimization) {
            _cgo_num_up = count_num_up();
        }

        // Perform interaction using the pre-selected interaction function
        apply_rule<IterateOver::vertices, Update::sync>(
            _interaction_func, _g, _state_cache
        );
        // NOTE Be careful, if adding another update rule after these:
        //      The `_num_up` member is incremented in the rules already,
        //      assuming that it determines the agent state _at the end_ of
        //      each iteration step.

        // Finally, build a history of the mean state
        track_mean_state_history();
    }


    /// Monitor model information
    /** This is computed in certain (wall) time intervals and can be used to
     *  estimate the state of the system during runtime.
     *  The mean state fluctuation is also used as a stop condition.
     */
    void monitor () {
        this->_monitor.set_entry(
            "mean_state", _mean_state_history.back()
        );

        if (_repetitions_enabled) {
            this->_monitor.set_entry(
                "repetition_no", _repetition_no
            );
        }

        // Mean state history is only computed and monitored, if the number of
        // samples is sufficiently low. Otherwise, return +inf
        if (_mean_state_history.size() >= _fluc_min_samples) {
            this->_monitor.set_entry(
                "mean_state_fluctuation",
                mean_state_fluctuation(_fluc_num_samples)
            );
        }
        else {
            this->_monitor.set_entry(
                "mean_state_fluctuation", std::numeric_limits<double>::infinity
            );
        }
    }


    /// Write data
    void write_data () {
        // Store the mean state (i.e.: the magnetization)
        // Can re-use the already computed mean state value from the history.
        _dset_mean_state->write(_mean_state_history.back());

        if (_write_external_field) {
            _dset_external_field->write(_external_field);
        }

        if (_write_interaction_energy) {
            _dset_interaction_energy_1->write(interaction_energy_1());
            _dset_interaction_energy_2->write(interaction_energy_2());
        }

        if (_write_cc) {
            const auto& cc_mean_state = _cc_mean_state_history.back();
            _dset_cc_mean_state->write(
                cc_mean_state.begin(), cc_mean_state.end(),
                [](const auto v){ return v; }
            );
        }

        if (_write_agent_data) {
            auto [v, v_end] = boost::vertices(_g);
            _dset_state->write(
                v, v_end,
                [this](auto vd) -> char {  // smallest available HDF5 type
                    if (_g[vd].state.up) {
                        return 1;
                    }
                    return -1;
                }
            );
        }
    }

    /// Invoked at the end of a simulation
    void epilog () override {
        this->_log->info("Epilog invoked.");
        if (_write_stable_state_mean_and_std) {
            const auto& [mean, stddev] = write_stable_state_mean_and_std();
            this->_log->info(
                "  Magnetization:   {:.3g} ± {:.3g}  (from last {} steps)",
                mean, stddev, _stable_state_num_samples
            );
        }
        if (_write_bias) {
            write_bias();
        }
        this->_log->info("Epilog finished.");
    }


    // .. Helper functions ....................................................

    /// Resets the agent states in prepeation for a new repetition
    bool maybe_start_new_repetition () {
        if (not _repetitions_enabled or this->_time == 0) {
            return false;
        }
        else if (this->_time % _repetition_interval != 0) {
            return false;
        }

        this->_log->info("Finished repetition #{}.", _repetition_no);

        // Store previous results
        // NOTE For the last repetition, this is done in the epilog!
        if (_write_stable_state_mean_and_std) {
            const auto& [mean, stddev] = write_stable_state_mean_and_std();
            this->_log->info(
                "  Magnetization:   {:.3g} ± {:.3g}  (from last {} steps)",
                mean, stddev, _stable_state_num_samples
            );
        }

        if (_write_bias) {
            write_bias();
        }

        // Prepare for next repetition
        _repetition_no++;

        this->_log->info(
            "Re-initializing agent states for repetition #{:d} @ time {} ...",
            _repetition_no, this->_time
        );
        this->init_agents(_g, get_as<Config>("init", this->_cfg));

        // Clear some history parameters
        _mean_state_history.clear();

        return true;
    }

    /// Stores the stable state mean and std in the corresponding
    auto write_stable_state_mean_and_std () const {
        const auto& [mean, stddev] = mean_state_and_stddev(
            _mean_state_history, _stable_state_num_samples
        );
        _dset_stable_state->write(std::vector<double>{mean, stddev});

        // Do the same for the largest connected component
        if (_write_cc) {
            auto gcc_mean_state_history = std::vector<double>();
            for (auto cc_m : _cc_mean_state_history) {
                gcc_mean_state_history.push_back(
                    cc_m[_largest_connected_component]
                );
            }

            const auto& [gcc_mean, gcc_stddev] = mean_state_and_stddev(
                gcc_mean_state_history, _stable_state_num_samples
            );
            _dset_cc_largest_component_stable_state->write(
                std::vector<double>{gcc_mean, gcc_stddev}
            );
        }

        return std::make_pair(mean, stddev);
    }

    auto write_bias () {
        auto [v, v_end] = boost::vertices(_g);
        _dset_bias->write(
            v, v_end,
            [this](auto vd) {
                return _g[vd].state.bias;
            }
        );
    }

    /// Returns a uniformly random number from the unit interval
    /** Used for evaluating probabilities
     */
    auto rand () {
        return this->_uniform_prob_distr(*this->_rng);
    }

    /// Computes the mean state and adds it to the history container
    void track_mean_state_history () {
        // Ensure we have not done this already; would be a mess otherwise
        // The +1 is there because the time is only incremented after
        // perform_step finished.
        if (_mean_state_history.size() > this->_time + 1) {
            throw std::runtime_error(
                "Mean state was already tracked in this time step! "
                "Can only do it once per time step."
            );
        }

        _mean_state_history.push_back(mean_state());

        // Component-specific mean state history, if enabled
        if (_num_cc) {
            _cc_mean_state_history.push_back(component_mean_states());
        }
    }

    /// Returns true if the configuration effectively set a complete graph
    bool complete_graph_configured (const Config& cfg) const {
        const auto model = get_as<std::string>("model", cfg);
        if (model == "complete") {
            return true;
        }
        else if (
            model == "ErdosRenyi_GNP" and
            get_as<float>("p", cfg["ErdosRenyi_GNP"]) >= 1.0
        ) {
            return true;
        }
        return false;
    }

    // .. Calculations ........................................................

    /// Computes the social field for an agent `v` given a specific bias value
    /** Iterates over neighbors of `v` and counts the number of agents in the
     *  `up` and the `down` state. The social field is then computed using
     *  these numbers and the respective biases.
     */
    auto compute_social_field (const VertexDesc v,
                               const GraphType& g,
                               const double bias) const
    {
        auto n_up = 0u;
        auto n_down = 0u;

        if (not _complete_graph_optimization) {
            // Need to count for this specific set of neighbors
            for (const auto& v2 : range<IterateOver::neighbors>(v, g)) {
                if (g[v2].state.up) {
                    n_up++;
                }
                else {
                    n_down++;
                }
            }
        }
        else {
            // Can use the mean field value instead, which is the same for all.
            // Only need to adjust it by excluding the state of the agent
            // itself, effectively ensuring that there is no self-edge:
            n_up = _cgo_num_up - g[v].state.up;
            n_down = (_N - 1) - n_up;
            // Examples for a 50/50 case with this agent in *-marked group:
            //      up  dwn
            //      50*/50 :  n_up = 50 - true = 49;  n_down = 99 - 49 = 50
            //      50 /50*:  n_up = 50 - false = 50; n_down = 99 - 50 = 49
        }

        return (bias * n_up - n_down) / (bias * n_up + n_down);
    }

    /// Computes the social field for `v` using the agent's inherent bias value
    auto compute_social_field (const VertexDesc v, const GraphType& g) const {
        return compute_social_field(v, g, g[v].state.bias);
    }

    /// Whether there should be a state flip due to the external field
    /** The field may be positive or negative, e.g. for time-varying fields.
     *
     *  As a simplification, we don't need to evaluate the external field at
     *  all if the agent is already in the state it would flip _towards_.
     */
    bool should_flip_from_external_field (const AgentState& state) {
        if (_external_field != 0) {
            if (_external_field > 0 and not state.up) {
                if (rand() < _external_field) {
                    return true;
                }
            } else if (_external_field < 0 and state.up) {
                if (rand() < std::fabs(_external_field)) {
                    return true;
                }
            }
        }
        return false;
    }


    // ..Rule functions .......................................................
    /// Applies social interaction and random state flips
    /** This rule is using the "old" noise mechanism, which will *not* flip to
     *  a *random* state but simply *flip*.
     *  The corresponding noise mode is called `flip`.
     *
     *  Additionally, the external field is applied.
     *
     *  \note This rule combines the social and noise processes and thus avoids
     *        another iteration over the graph (and the accompanying state
     *        cache updates). It is thus faster than two separate rules.
     */
    VertexRule _interact_and_flip_randomly =
        [this](const VertexDesc v, GraphType& g)
    {
        // Get a copy of the current state.
        // This copy will then be adapted to the new state.
        auto state = g[v].state;

        // -- Social Interaction
        // Determine social field, then compute probability to actually flip
        auto p_flip = this->_ia_sigmoid(compute_social_field(v, g));
        // NOTE This is the probability to flip **_to_ the up state** (from
        //      the down state)!
        //      If the agent is in the up state, we need to look at 1 - p_flip!
        if (state.up) {
            p_flip = 1.0 - p_flip;
        }

        // Evaluate the state change
        if (rand() < p_flip) {
            state.flip();
        }
        // else: no state change due to the social field

        // -- Random state change
        // Note that this happens _on top_ of a potential previous change in
        // state, thus introducing a dependency. For high noise levels, this
        // will reduce stochasticity (unlike for `random_state` noise mode).
        if (_p_flip_random > 0 and rand() < _p_flip_random) {
            state.flip();
        }

        // -- State change in response to weak external field
        if (this->should_flip_from_external_field(state)) {
            state.flip();
        }

        // Track the new state value (avoids repeated iteration)
        this->_num_up += state.up;

        return state;
    };

    /// Applies social interaction and noise-induced changes to a random state
    /** This rule is using the "new" noise mechanism, where a noise event will
     *  cause a state change to a *random* state.
     *  The corresponding noise mode is called `random_state`.
     *
     *  Additionally, the external field is applied.
     *
     *  \note This rule combines the social and noise processes and thus avoids
     *        another iteration over the graph (and the accompanying state
     *        cache updates). It is thus faster than two separate rules.
     */
    VertexRule _interact_and_apply_noise =
        [this](const VertexDesc v, GraphType& g)
    {
        // Get a copy of the current state.
        // This copy will then be adapted to the new state.
        auto state = g[v].state;

        // Either flip to a random state -- or interact socially
        // NOTE Having this "either-or" type of implementation is a performance
        //      improvement for the case where there would be a noise event:
        //      In case of a noise event, the state will be random anyway, so
        //      there is no point in computing the transition due to the social
        //      field. This way, we avoid iterating over the neighbors.
        //
        //      This implementation is equivalent to _always_ doing the social
        //      field update and then _sometimes_ (according to p_noise) having
        //      a noise event.
        if (_p_noise > 0 and rand() < _p_noise) {
            // -- Noise event: change to a random state, i.e. 50% probability
            // This is independent of the _previous_ state.
            state.up = (rand() < 0.5);
        }
        else {
            // -- Social interaction
            // Determine social field, computing probability to actually flip
            auto p_flip = this->_ia_sigmoid(compute_social_field(v, g));
            // NOTE This is the probability to flip **_to_ the up state**
            //      (from the down state)! If the agent is in the up state, we
            //      need to look at 1 - p_flip!
            if (state.up) {
                p_flip = 1.0 - p_flip;
            }

            // Evaluate the state change
            if (rand() < p_flip) {
                state.flip();
            }
            // else: no state change due to the social field
        }

        // -- State change in response to weak external field
        if (this->should_flip_from_external_field(state)) {
            state.flip();
        }

        // Track the new state value (avoids repeated iteration)
        this->_num_up += state.up;

        return state;
    };


    // -- Observables ---------------------------------------------------------

    /// Counts the current number of up-states
    /** The number of down states is simply ``_N - n_up``.
     */
    auto count_num_up () const {
        auto n_up = 0u;
        for (const auto& v : range<IterateOver::vertices>(_g)) {
            if (_g[v].state.up) {
                n_up++;
            }
        }
        return n_up;
    }

    /// Calculates the mean state of all agents (in [-1, +1] interval)
    /** Also referred to as magnetization.
     *
     *  \WARNING This uses the `_num_up` counter, which is updated as part of
     *           the state update; there is no separate iteration!
     *           Meaning: If this is invoked _during_ the update, the result
     *           will not be right!
     *  \TODO    Consider putting _num_up in "invalid" state to prohibit this.
     */
    auto mean_state () const {
        // Translate number of up states to value in [-1, +1] interval
        return ((2. * _num_up) / _N) - 1.;
    }

    /// Calculates mean states for each individual components
    /** Unlike the main `mean_state` method, this uses a separate iteration
     *  over all nodes.
     */
    auto component_mean_states () const {
        using Utopia::Utils::in_range;

        const auto& cc = _connected_components;
        const auto& cc_sizes = _connected_component_sizes;

        if (not _num_cc) {
            throw std::runtime_error(
                "Attempted to compute connected component mean states, but "
                "connected components were not determined!"
            );
        }

        auto n_up = std::vector<std::size_t>(_num_cc, 0);
        for (const auto& v : range<IterateOver::vertices>(_g)) {
            if (_g[v].state.up) {
                n_up[cc[v]]++;
            }
        }

        auto mean_states = std::vector<double>(_num_cc, 0.);
        for (auto i : in_range(_num_cc)) {
            mean_states[i] = ((2. * n_up[i]) / cc_sizes[i]) - 1.;
        }
        return mean_states;
    }

    auto compute_number_opposed_neighbors (const VertexDesc v,
                                           const GraphType& g) const
    {
        auto n_up = 0u;
        auto n_down = 0u;
        for (const auto& v2 : range<IterateOver::neighbors>(v, g)) {
            if (g[v2].state.up) {
                n_up++;
            }
            else {
                n_down++;
            }
        }
        if (g[v].state.up) {
            return (n_down);
        } else {
            return (n_up);
        }
    }

    auto compute_fraction_opposed_neighbors (const VertexDesc v,
                                             const GraphType& g) const
    {
        double n_up = 0;
        double n_down = 0;
        for (const auto& v2 : range<IterateOver::neighbors>(v, g)) {
            if (g[v2].state.up) {
                n_up++;
            }
            else {
                n_down++;
            }
        }
        if (g[v].state.up) {
            return (n_down/(n_up+n_down));
        } else {
            return (n_up/(n_up+n_down));
        }
    }

    auto interaction_energy_1 () const {
        using Utopia::range;
        using Utopia::IterateOver;
        double sum_of_opposed_neighbors = 0;
        double number_of_edges = boost::num_edges(_g);

        for (const auto& v : range<IterateOver::vertices>(_g)) {
            auto number_opposed_neighbors =
                compute_number_opposed_neighbors(v, _g);
            sum_of_opposed_neighbors += number_opposed_neighbors;
        }

        double interaction_energy_1 =
            sum_of_opposed_neighbors / number_of_edges;
        return interaction_energy_1 / 2;
    }

    auto interaction_energy_2 () const {
        using Utopia::range;
        using Utopia::IterateOver;
        double sum_of_opposed_neighbor_fractions = 0;
        double number_of_nodes = 0;

        for (const auto& v : range<IterateOver::vertices>(_g)) {
            auto fraction_opposed_neighbors =
                compute_fraction_opposed_neighbors(v, _g);
            sum_of_opposed_neighbor_fractions += fraction_opposed_neighbors;
            number_of_nodes += 1;
        }

        double interaction_energy_2 =
            sum_of_opposed_neighbor_fractions / number_of_nodes;
        return interaction_energy_2;
    }

    // TODO Online Running mean algorithm?!

    /// Computes the mean state and std. dev. from the last T steps of history
    template<class C>
    auto mean_state_and_stddev (const C& history, std::size_t T) const {
        const auto& ms = history;
        T = std::clamp(T, T, ms.size());

        // mean
        const double sum = std::accumulate(
            Utopia::Utils::from_the_back(ms, T), std::end(ms), 0.0
        );
        const auto mean = sum / T;

        // stddev
        auto diff = std::vector<double>(T);
        std::transform(
            Utopia::Utils::from_the_back(ms, T), std::end(ms),
            diff.begin(),
            [mean](double m) { return m - mean; }
        );
        double sq_sum = std::inner_product(
            diff.begin(), diff.end(),
            diff.begin(),
            0.0
        );
        const double stddev = std::sqrt(sq_sum / T);

        return std::make_pair(mean, stddev);
    }

    /// Returns a quantity that denotes the fluctuation in mean state history
    /** Basically: the maximum value of the absolute deviation from the mean.
     *
     *  \param N    The number of samples (from the history) to use. If larger
     *              than the number of available samples, will clamp to the
     *              size of the mean state history container.
     *
     *  \warning    This does not test whether there are sufficiently many
     *              samples to make a good judgement of the mean state
     *              fluctuations; you have to do that yourself!
     */
    auto mean_state_fluctuation (std::size_t N) const {
        const auto& ms = _mean_state_history;

        N = std::clamp(N, N, ms.size());
        const double sum = std::accumulate(
            Utopia::Utils::from_the_back(ms, N), std::end(ms), 0.0
        );
        const double mean =  sum / N;

        auto abs_dev = 0.;
        auto max_abs_deviation = 0.;
        std::for_each(
            Utopia::Utils::from_the_back(ms, N), std::end(ms),
            [&](const double d) {
                abs_dev = std::fabs(mean - d);
                if (abs_dev > max_abs_deviation) {
                    max_abs_deviation = abs_dev;
                }
            }
        );

        return max_abs_deviation;
    }

};

} // namespace Utopia::Models::SocialSpinModel

#endif // UTOPIA_MODELS_SOCIALSPINMODEL_HH
