#ifndef UTOPIA_MODELS_SOCIALSPINMODEL_ENTITIES_HH
#define UTOPIA_MODELS_SOCIALSPINMODEL_ENTITIES_HH


#include <utopia/core/graph.hh>


namespace Utopia::Models::SocialSpinModel {

/// Agents sit on each vertex
struct AgentState {
    bool up;
    float bias;

    /// Return a copy of this struct with the `up` state flipped
    AgentState flipped () const {
        return {not up, bias};
    }

    /// Flip the state (in-place!)
    void flip () {
        up = not up;
    }
};

/// Links between agents (i.e. edges), enabling interactions
/** \note Currently not in use -- TODO could add something here if needed.
 */
struct LinkState {

};

} // namespace

#endif // UTOPIA_MODELS_SOCIALSPINMODEL_ENTITIES_HH
