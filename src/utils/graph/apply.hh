#ifndef UTOPIA_SSM_UTILS_APPLY_HH
#define UTOPIA_SSM_UTILS_APPLY_HH

// TODO This should be implemented upstream in Utopia; remove this header once
//      that is the case (see utopia!304)

#include <type_traits>
#include <boost/graph/graph_traits.hpp>

#include <utopia/core/graph/iterator.hh>
#include <utopia/core/state.hh>


namespace Utopia {

namespace GraphUtils {
/// Apply a rule synchronously using a previously created state cache
/** This helper function applies a rule to a range of entities that is given
 *  through an iterator pair.
 *  A state cache is created that stores the returned states of the rule
 *  function. After the rule was applied to each graph entity within the
 *  iterator range the cached states are moved to the actual states of the
 *  graph entities, thus, updating their states synchronously.
 *
 * \tparam Iter     The iterator type
 * \tparam Graph    The graph type
 * \tparam Rule     The rule type
 *
 * \param it_begin  The begin of the graph entity iterator range.
 * \param it_end    The end of the graph entity iterator range.
 * \param g         The graph
 * \param rule      The rule function to be applied to each element within the
 *                  iterator range.
 * \param state_cache A previously created container that will be used to hold
 *                  the cached state. In the process, the content will be
 *                  cleared!
 *
 * \warning Be careful to not operate directly on the state of a graph entity
 *          within the rule function. Rather, first create a copy of the state
 *          and return the copied and changed state at the end of the function.
 */
template<typename Iter, typename Graph, typename Rule, typename StateCache>
void apply_sync(Iter it_begin,
                Iter it_end,
                Graph&& g,
                Rule&& rule,
                StateCache&& state_cache)
{
    // Clear the cache
    state_cache.clear();

    // apply the rule
    for (auto entity : boost::make_iterator_range(it_begin, it_end)){
        state_cache.push_back(rule(entity, g));
    }

    // move the cache
    auto counter = 0u;
    for (auto entity : boost::make_iterator_range(it_begin, it_end)){
        g[entity].state = std::move(state_cache[counter]);

        ++counter;
    }
}
} // namespace GraphUtils



/// Synchronously apply a rule to graph entities, re-using a cache container
/** This overload specifies the apply_rule function for a synchronous update.
 *  In such a case, it makes no sense to shuffle, so the shuffle option is not
 *  available here.
 *
 * \tparam iterate_over Over which kind of graph entity to iterate over. See
 *                      \ref IterateOver
 * \tparam mode         The update mode, see \ref UpdateMode
 * \tparam Graph        The graph type
 * \tparam Rule         The rule type
 *
 * \param rule          The rule function, expecting (descriptor, graph)
 *                      as arguments. For the synchronous update, the rule
 *                      function needs to return the new state.
 * \param g             The graph
 * \param state_cache   The state cache container that is to be re-used
 */
template<IterateOver iterate_over,
         Update mode,
         typename Graph,
         typename Rule,
         typename StateType,
         typename std::enable_if_t<mode == Update::sync, int> = 0>
void apply_rule(Rule&& rule, Graph&& g, std::vector<StateType>& state_cache)
{
    using namespace GraphUtils;
    auto [it, it_end] = iterator_pair<iterate_over>(g);
    apply_sync(it, it_end, g, rule, state_cache);
}



} // namespace Utopia

#endif // UTOPIA_SSM_UTILS_APPLY_HH
