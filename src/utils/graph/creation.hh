#ifndef UTOPIA_MODELS_SSM_GRAPH_CREATION_HH
#define UTOPIA_MODELS_SSM_GRAPH_CREATION_HH

// NOTE This expands on the graph creation algorithms implemented in Utopia.
//      Ideally, these should be implemented upstream ...

#include <algorithm>
#include <cmath>
#include <exception>
#include <iterator>
#include <ranges>
#include <random>
#include <stdexcept>
#include <utopia/core/grids/base.hh>
#include <vector>
#include <deque>
#include <string>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/small_world_generator.hpp>
#include <boost/graph/random.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/dynamic_property_map.hpp>

// #include <fmt/core.h>

#include <utopia/core/graph/iterator.hh>
#include <utopia/data_io/cfg_utils.hh>
#include <utopia/core/types.hh>
#include <utopia/core/graph.hh>
#include <utopia/core/grids.hh>
#include <utopia/core/space.hh>
#include <UtopiaUtils/utils/iterators.hh>
#include <UtopiaUtils/utils/select.hh>


namespace Utopia::Utils::Graph {


// -- Some helper functions ---------------------------------------------------

template<class Graph, class RNG>
Graph& reduce_degree (
    Graph& g,
    const double mean_degree,
    RNG& rng,
    bool cut_locally = false
) {
    using VertexDesc = typename boost::graph_traits<Graph>::vertex_descriptor;
    using SourceTargetPair = std::pair<VertexDesc, VertexDesc>;

    // How many edges do we need? (Assuming an undirected graph)
    // TODO allow directed graphs (need to adjust mean degree calculation)
    if (boost::is_directed(g)) {
        throw std::invalid_argument(
            "Cannot reduce degree of directed graphs yet!"
        );
    }

    const auto num_vertices = static_cast<int>(boost::num_vertices(g));
    const auto num_edges = static_cast<int>(boost::num_edges(g));
    const auto desired_num_edges = std::round(
        mean_degree * num_vertices / 2.
    );
    const int delta_num_edges = desired_num_edges - num_edges;

    std::cout << "edges before: " << num_edges << std::endl;
    std::cout << "edges desired: " << desired_num_edges << std::endl;
    std::cout << "delta num edges: " << delta_num_edges << std::endl;

    if (delta_num_edges > 0) {
        // Make sure there are sufficient edges, otherwise we cannot cut
        throw std::invalid_argument(fmt::format(
            "Do not have sufficient edges ({}) to reduce the given graph "
            "of {} nodes (mean degree {}) to a mean degree of {}! "
            "Specify a graph with a larger mean degree to allow reducing "
            "the mean degree.",
            num_edges,
            num_vertices,
            2. * num_edges / num_vertices,
            mean_degree
        ));
    }
    else if (delta_num_edges == 0) {
        return g;
    }

    // else: Randomly cut links.
    // TODO Implement locally cutting links instead of globally
    if (cut_locally) {
        throw std::invalid_argument("`cut_locally` not implemented!");
    }

    const std::size_t num_edges_to_cut = std::abs(delta_num_edges);
    std::cout << "edges to cut: " << num_edges_to_cut << std::endl;

    // The edge iterator of the graph is typically not a random access
    // iterator, so we cannot sample from it. Instead, we need to use
    // their index, making this a bit more tedious ...
    auto edge_idcs_to_cut = std::deque<std::size_t>{};
    edge_idcs_to_cut.resize(num_edges_to_cut);
    std::ranges::sample(
        std::views::iota(0u, static_cast<std::size_t>(num_edges)),
        edge_idcs_to_cut.begin(),
        num_edges_to_cut,
        rng
    );

    // Translate to actual edges that are to be removed
    std::cout << "determining edges to cut ... " << std::endl;
    auto edges_to_cut_st = std::vector<SourceTargetPair>{};
    auto edge_idx = 0u;
    for (const auto ed : range<IterateOver::edges>(g)) {
        if (edge_idx == edge_idcs_to_cut.front()) {
            edges_to_cut_st.push_back(
                {boost::source(ed, g), boost::target(ed, g)}
            );
            edge_idcs_to_cut.pop_front();
        }
        if (edge_idcs_to_cut.empty()) {
            break;
        }
        edge_idx++;
    }

    // TODO use select instead (but fails currently):
    // const auto edges_to_cut = Utopia::Utils::select(
    //     range<IterateOver::edges>(g),
    //     num_edges_to_cut,
    //     "random",
    //     rng
    // );
    // auto edges_to_cut_st = std::vector<SourceTargetPair>{};
    // edges_to_cut_st.resize(num_edges_to_cut);
    // std::transform(
    //     edges_to_cut.begin(), edges_to_cut.end(), edges_to_cut_st.begin(),
    //     [&](auto ed) -> SourceTargetPair {
    //         return {boost::source(ed, g), boost::target(ed, g)};
    //     }
    // );

    std::cout << "removing edges ... " << std::endl;
    for (const auto& [u, v] : edges_to_cut_st) {
        boost::remove_edge(u, v, g);
        std::cout << "  removed edge " << u << "->" << v << std::endl;
    }

    return g;
}






// -- Additional graph creation algorithms ------------------------------------
namespace Creators {

/// Returns a G_{n, p} random graph, aka: Erdős-Rényi graph, binomial graph
/** The algorithm chooses each of the ``n(n-1)/2`` (undirected) or ``n(n-1)``
 *  (directed) possible edges with possibility ``p``.
 *
 *  \tparam Graph           The graph type to create
 *
 *  \param  num_vertices    Number of vertices in the graph
 *  \param  p               Probability for an edge to exist
 *  \param  self_edges      Whether self-loops should be included
 *  \param  rng             The RNG object used to evaluate the probabilities
 */
template<typename Graph, typename RNG>
Graph create_gnp_random_graph(
    const std::size_t num_vertices,
    const double p,
    bool self_edges,
    RNG& rng
){
    using Utopia::range;
    using Utopia::IterateOver;

    if (p < 0 or p > 1) {
        throw std::invalid_argument(
            "Edge creation probability needs to be in [0, 1], but was: "
            + std::to_string(p)
        );
    }

    // Create graph with the specified number of vertices
    Graph g(num_vertices);

    // Setup functional that evaluates whether to add an edge
    auto prob_distr = std::uniform_real_distribution<double>(0., 1.);
    auto maybe_add_edge = [&](const auto& u, const auto& v, auto& g){
        // May want to skip self-edges
        if (not self_edges and u == v) {
            return;
        }

        // Add this edge (with a certain probability)
        if (prob_distr(rng) < p) {
            boost::add_edge(u, v, g);
        }
    };

    // Depending on directedness, iterate differently over the graph
    if (boost::is_directed(g)) {
        // Directed: iterate over all combinations
        for (const auto& u : range<IterateOver::vertices>(g)) {
            for (const auto& v : range<IterateOver::vertices>(g)) {
                maybe_add_edge(u, v, g);
            }
        }
    }
    else {
        // Undirected: iterate over all permutations
        // TODO Is there a more elegant iterator-based solution for this?
        for (const auto& u : range<IterateOver::vertices>(g)) {
            for (const auto& v : range<IterateOver::vertices>(g)) {
                if (v < u) {
                    continue;
                }
                maybe_add_edge(u, v, g);
            }
        }
    }

    // TODO Implement option to randomly reconnect isolated components.
    //      This would slightly change the degree distribution, but nothing
    //      too drastic.

    return g;
}


// TODO Actually implement this faster version
/// Returns a G_{n, p} random graph, aka: Erdős-Rényi graph, binomial graph
/** The algorithm chooses each of the ``n(n-1)/2`` (undirected) or ``n(n-1)``
 *  (directed) possible edges with possibility ``p``.
 *
 *  This algorithm runs in $O(n + m)$ time, where `m` is the expected number of
 *  edges, which equals $p n (n - 1) / 2$. This should be faster than
 *  create_gnp_random_graph for sparse graphs.
 *
 *  References:
 *      Vladimir Batagelj and Ulrik Brandes,
 *      "Efficient generation of large random networks",
 *      Phys. Rev. E, 71, 036113, 2005.
 *
 *  \tparam Graph           The graph type to create
 *
 *  \param  num_vertices    Number of vertices in the graph
 *  \param  p               Probability for an edge to exist
 *  \param  self_edges      Whether self-loops should be included
 *  \param  rng             The RNG object used to evaluate the probabilities
 */
template<typename Graph, typename RNG>
Graph create_gnp_random_graph_fast(
    [[maybe_unused]] const std::size_t num_vertices,
    [[maybe_unused]] const double p,
    [[maybe_unused]] bool self_edges,
    [[maybe_unused]] RNG& rng
){
    using Utopia::range;
    using Utopia::IterateOver;

    throw std::invalid_argument(
        "Not implemented yet: create_gnp_random_graph_fast"
    ); // FIXME remove once implemented

    // if (p < 0 or p > 1) {
    //     throw std::invalid_argument(
    //         "Edge creation probability needs to be in [0, 1], but was: "
    //         + std::to_string(p)
    //     );
    // }

    // // Create graph with the specified number of vertices
    // Graph g(num_vertices);

    // // Setup functional that evaluates whether to add an edge
    // auto prob_distr = std::uniform_real_distribution<double>(0., 1.);
    // const auto log_p = std::log(1.0 - p);

    // // Depending on directedness, iterate differently over the graph
    // if (boost::is_directed(g)) {
    //     // Directed: add (v, u) edges
    //     // TODO
    // }

    // // Add (u, v) edges
    // // TODO

    // return g;
}



/// Constructs a 2D lattice graph
/** The graph will have ``num_rows * num_cols`` vertices.
 *  For directed graphs, there will be antiparallel edges.
 *  With ``periodic`` set, vertices in the last and first columns and rows will
 *  be connected to their counterparts, thus representing a torus topology.
 *
 *  \tparam Graph           The graph type to create
 *
 *  \param  num_cols        Number of columns of the grid
 *  \param  num_rows        Number of rows of the grid
 *  \param  self_edges      Whether self-loops should be included
 *  \param  periodic        Whether boundary nodes should be connected to their
 *                          counterparts at the corresponding other boundary,
 *                          thus creating a torus topology.
 *  \param  num_neighbors   The number of neighbors to add; currently only
 *                          supports von-Neumann neighborhoods (4)
 */
template<typename Graph>
Graph create_lattice_graph(
    const std::size_t num_cols,
    const std::size_t num_rows,
    const bool self_edges,
    const bool periodic,
    const std::size_t num_neighbors = 4
){
    using Utopia::Utils::in_range;

    using VertexDesc = typename boost::graph_traits<Graph>::vertex_descriptor;
    using EdgeDesc = typename boost::graph_traits<Graph>::edge_descriptor;

    if (num_neighbors != 4) {
        throw std::invalid_argument(
            "Number of neighbors for lattice graph needs to be 4, was "
            + std::to_string(num_neighbors)
        );
    }
    if (num_cols < 2 or num_rows < 2) {
        throw std::invalid_argument(
            "Number of columns or rows of lattice graph needs to be at "
            "least two, but was " + std::to_string(num_cols) + " and "
            + std::to_string(num_rows) + " respectively."
        );
    }

    // Create a graph with the specified number of vertices
    const auto num_vertices = num_cols * num_rows;
    Graph g(num_vertices);
    // TODO … consider using boost::grid_graph maybe? But that sure has some
    //        limitations.

    // Define some helper functions
    // ... to get a vertex descriptor from a column and row index
    auto get_vertex_desc = [&](auto col, auto row) -> VertexDesc {
        return boost::vertex(col + (num_cols * row), g);
    };

    // ... to get all neighbors for a vertex at the given column and row index.
    //     This can be solved more generally, especially if extending this
    //     to a wider neighborhood.
    auto get_neighbors = [&](auto col, auto row) -> std::vector<VertexDesc> {
        std::vector<VertexDesc> neighbors{};

        if (periodic) {
            // first axis: same row
            neighbors.push_back(get_vertex_desc((col - 1) % num_cols, row));
            neighbors.push_back(get_vertex_desc((col + 1) % num_cols, row));

            // second axis: same column
            neighbors.push_back(get_vertex_desc(col, (row - 1) % num_rows));
            neighbors.push_back(get_vertex_desc(col, (row + 1) % num_rows));
        }
        else {
            // For non-periodic boundary, need to exclude some connections
            // first axis: same row
            if (col >= 1) {
                neighbors.push_back(get_vertex_desc(col - 1, row));
            }
            if (col + 1 < num_cols) {
                neighbors.push_back(get_vertex_desc(col + 1, row));
            }

            // second axis: same column
            if (row >= 1) {
                neighbors.push_back(get_vertex_desc(col, row - 1));
            }
            if (row + 1 < num_rows) {
                neighbors.push_back(get_vertex_desc(col, row + 1));
            }
        }

        return neighbors;
    };

    // Go over columns and rows and add the respective neighbors
    VertexDesc u;
    EdgeDesc e;
    const auto is_directed = boost::is_directed(g);

    for (auto col : in_range(num_cols)) {
        for (auto row : in_range(num_rows)) {
            u = get_vertex_desc(col, row);
            if (self_edges) {
                boost::add_edge(u, u, g);
            }

            for (const auto& v : get_neighbors(col, row)) {
                auto [e, exists] = boost::edge(u, v, g);
                if (not exists) {
                    boost::add_edge(u, v, g);
                }

                if (is_directed) {
                    auto [e, exists] = boost::edge(v, u, g);
                    if (not exists) {
                        boost::add_edge(v, u, g);
                    }
                }
            }
        }
    }

    return g;
}


/// Constructs a 2D grid graph using Utopia::Grid
/** This allows to interface with different neighborhoods and grid types very
 *  easily.
 *
 *  \note The ``num_vertices`` and ``mean_degree`` parameters is ignored here!
 */
template <typename Graph>
Graph create_grid_graph(
    const std::size_t num_vertices,
    const double mean_degree,
    const Config& cfg,
    const bool self_edges = false
){
    using namespace Utopia::Graph;
    using Utopia::get_as;
    using Utopia::Utils::in_range;

    using VertexDesc = typename boost::graph_traits<Graph>::vertex_descriptor;
    using EdgeDesc = typename boost::graph_traits<Graph>::edge_descriptor;
    using Space = Utopia::Space<2>;
    using Grid = Utopia::Grid<Space>;
    using Utopia::nb_mode_map;

    // Set up the space needed for the grid
    const auto space = std::make_shared<Space>(get_as<Config>("space", cfg));

    // Set up the grid, depending on chosen structure
    const auto structure = get_as<std::string>("structure", cfg);

    std::shared_ptr<Grid> grid;
    if (structure == "triangular") {
        grid = std::make_shared<TriangularGrid<Space>>(space, cfg);
    }
    else if (structure == "square") {
        grid = std::make_shared<SquareGrid<Space>>(space, cfg);
    }
    else if (structure == "hexagonal") {
        grid = std::make_shared<HexagonalGrid<Space>>(space, cfg);
    }
    else {
        throw std::invalid_argument(fmt::format(
            "Invalid value for grid 'structure' argument: '{}! "
            "Allowed values: 'square', 'hexagonal', 'triangular'", structure
        ));
    }

    // Check parameters; the number of vertices and degree is actually defined
    // by the grid parameters, so we should make sure that the given parameters
    // are not *silently* ignored but it's clearly communicated that these will
    // be ignored.
    const auto N = grid->num_cells();

    if (num_vertices > 0 and num_vertices != N) {
        throw std::invalid_argument(fmt::format(
            "For a {} grid graph, the number of vertices is specified via the "
            "grid parameters, not the `num_vertices` argument! Make sure to "
            "either specify 0 or the correct number of vertices ({}). "
            "Got: {}", structure, N, num_vertices
        ));
    }

    if (mean_degree >= 0.) {
        throw std::invalid_argument(fmt::format(
            "For a grid graph, the `mean_degree` parameter is ignored! "
            "Specify a negative value instead. Got: {}", mean_degree
        ));
    }

    // Select the neighborhood
    const auto nb_cfg = get_as<Config>("neighborhood", cfg);
    const auto nb_mode = get_as<std::string>("mode", nb_cfg);
    grid->select_neighborhood(nb_mode_map.at(nb_mode), nb_cfg);

    // Now, we have an ID-based grid that we can use to create a graph from.
    // Start with an empty graph
    auto g = Graph(N);

    // To be most general, we need to map grid IDs to vertex descriptors first
    auto vds = std::vector<VertexDesc>();
    vds.reserve(N);
    for (const auto vd : range<IterateOver::vertices>(g)) {
        vds.push_back(vd);
    }

    // Now, let's go through each grid cell and add links to each neighbor,
    // making sure that no parallel edges are added.
    VertexDesc u, v;
    EdgeDesc e;

    for (const auto i : in_range(N)) {
        u = vds[i];
        if (self_edges) {
            boost::add_edge(u, u, g);
        }

        for (const auto j : grid->neighbors_of(i)) {
            v = vds[j];
            auto [ed, exists] = boost::edge(u, v, g);
            if (not exists) {
                boost::add_edge(u, v, g);
                // std::cout << "  added edge " << u << "->" << v << std::endl;
            }
        }
    }

    return g;
}

/// Create a Watts-Strogatz small-world graph starting from a 2D grid
/** Constructs a small-world graph using the Watts-Strogatz algorithm.
 *  It first creates a 2D grid graph and then relocates vertex connections
 *  with a given probability.
 *  If desired, the graph is first pruned to arrive at a certain mean degree.
 *
 * \tparam Graph           The graph type
 * \tparam RNG             The random number generator type
 *
 * \param  num_vertices    The desired number of vertices; set to zero to let
 *                         the grid graph decide on the number, otherwise will
 *                         need to specify the correct number.
 * \param  mean_degree     The desired mean degree. If given, need to also set
 *                         the `cut_down_to_mean_degree` parameter. If the grid
 *                         graph should determine the initial mean degree, set
 *                         this to -1.
 * \param  p_rewire        The rewiring probability
 * \param  grid_cfg        The configuration for creating the underlying grid
 *                         graph. Needs to generate a graph with a sufficiently
 *                         high mean degree, if cutting to `mean_degree` is
 *                         desired.
 * \param  RNG             A random number generator instance
 * \param  cut_down_to_mean_degree  If true, will reduce the generated grid
 *                         graph to the given mean degree. If false, will use
 *                         the grid graph directly (requiring `mean_degree` set
 *                         to -1 to denote this).
 * \param  cut_locally     If true, will determine for each node how many edges
 *                         need to be removed (NOT IMPLEMENTED).
 * \param  rewire_fraction  If false, will decide for each edge whether it
 *                         should be rewired; if true, will regard `p_rewire`
 *                         as a fraction and sample from the edges.
 * \param  self_edges      Whether self-loops should be included
 * \param  max_trials      Number of retries if rewiring resulted in an already
 *                         existing edge or in an undesired self-edge
 *
 * \return Graph           The resulting grid-based small-world graph
 */
template <typename Graph, typename RNG>
Graph create_WattsStrogatz_grid_graph(
    const std::size_t num_vertices,
    const double mean_degree,
    const double p_rewire,
    const Config& grid_cfg,
    RNG& rng,
    const bool cut_down_to_mean_degree = true,
    [[maybe_unused]] const bool cut_locally = false, // TODO implement
    const bool rewire_fraction = true,
    const bool self_edges = false,
    const std::size_t max_trials = 32
){
    using namespace Utopia::Graph;
    using Utopia::range;

    using VertexDesc = typename boost::graph_traits<Graph>::vertex_descriptor;
    // using EdgeDesc = typename boost::graph_traits<Graph>::edge_descriptor;

    // Start with a grid graph
    auto g = create_grid_graph<Graph>(num_vertices, -1., grid_cfg, self_edges);

    if (cut_down_to_mean_degree) {
        if (mean_degree < 0) {
            throw std::invalid_argument(fmt::format(
                "{} is not a valid mean degree when `cut_down_to_mean_degree` "
                "is also given!", mean_degree
            ));
        }
        g = reduce_degree(g, mean_degree, rng);
    }
    else {
        if (mean_degree > 0) {
            throw std::invalid_argument(fmt::format(
                "Got `mean_degree` {} despite `cut_down_to_mean_degree` not "
                "being set!", mean_degree
            ));
        }
    }
    const auto num_edges = boost::num_edges(g);
    std::cout << "edges after cutting: " << num_edges << std::endl;

    // .. Rewiring procedure . . . . . . . . . . . . . . . . . . . . . . . . .
    if (p_rewire == 0.) {
        return g;
    }

    // TODO enhancement for p_rewire == 1.0?

    // Create a mapping from vertex index to descriptor
    auto vds = std::vector<VertexDesc>();
    vds.reserve(boost::num_vertices(g));
    for (const auto vd : range<IterateOver::vertices>(g)) {
        vds.push_back(vd);
    }

    // Determine which edges to rewire
    auto edges_to_rewire = std::vector<std::pair<VertexDesc, VertexDesc>>{};
    auto pdist = std::uniform_real_distribution<>(0., 1.);

    if (rewire_fraction) {
        auto edge_idcs_to_rewire = std::deque<std::size_t>{};
        auto num_edges_to_rewire = std::round(num_edges * p_rewire);
        edge_idcs_to_rewire.resize(num_edges_to_rewire);
        std::ranges::sample(
            std::views::iota(0u, num_edges),
            edge_idcs_to_rewire.begin(),
            num_edges_to_rewire,
            rng
        );

        auto edge_idx = num_edges;
        for (const auto ed : range<IterateOver::edges>(g)) {
            if (edge_idx == edge_idcs_to_rewire.front()) {
                edges_to_rewire.push_back(
                    {boost::source(ed, g), boost::target(ed, g)}
                );
                edge_idcs_to_rewire.pop_front();
            }
            if (edge_idcs_to_rewire.empty()) {
                break;
            }
            edge_idx++;
        }
    }
    else {
        for (auto ed : range<IterateOver::edges>(g)) {
            if (pdist(rng) < p_rewire) {
                edges_to_rewire.push_back(
                    {boost::source(ed, g), boost::target(ed, g)}
                );
            }
        }
    }

    // Define a functional to find a new random target vertex for an edge
    auto vertex_index_distr = std::uniform_int_distribution<std::size_t>(
        0, boost::num_vertices(g) - 1
    );
    auto new_random_target_vertex_for_edge = [&](auto s, auto t){
        auto trials = 0u;
        while (
            boost::edge(s, t, g).second      // edge s->t already exists
            or (not self_edges and s == t)   // would be self-edge (undesired)
        ) {
            // Determine a new random target index
            t = vds[vertex_index_distr(rng)];

            trials++;
            if (trials > max_trials) {
                throw std::runtime_error(
                    "Failed to find an edge that does not yet exist!"
                );
            }
        }
        // std::cout << "  found new target after " << trials << " trials"
        //     << std::endl;
        return t;
    };

    // Now actually perform the rewiring
    std::cout << "rewiring " << edges_to_rewire.size() << " edges ..."
        << std::endl;

    VertexDesc s, t;
    for (auto [u, v] : edges_to_rewire) {
        // Determine which vertex should be kept
        if (pdist(rng) < 0.5) {
            s = u;
            t = v;
        }
        else {
            s = v;
            t = u;
        }

        // Determine new target vertex that creats an edge that does NOT exist
        t = new_random_target_vertex_for_edge(s, t);

        // Remove old edge and add the new one
        boost::remove_edge(u, v, g);
        boost::add_edge(s, t, g);

        // std::cout << "  replaced " << u << "--" << v;
        // std::cout << "  with " << s << "--" << t << std::endl;
    }

    // Make sure nothing went wrong
    if (boost::num_edges(g) != num_edges) {
        throw std::runtime_error(fmt::format(
            "Something went wrong during rewiring of {} edges such that the "
            "total edge number of the graph with {} nodes was not preserved! "
            "Had {} edges before rewiring and {} after.",
            edges_to_rewire.size(), num_vertices, num_edges,
            boost::num_edges(g)
        ));
    }

    // Done.
    return g;
}


} // namespace …::Creators


// -- Convenience wrappers ----------------------------------------------------
/// Create a graph from a configuration node
/** This overloads Utopia::create_graph by adding further graph models.
 *
 *  Select a graph creation algorithm and create the graph object a
 *  configuration node.
 *
 * \tparam Graph        The graph type
 * \tparam Config       The configuration node type
 * \tparam RNG          The random number generator type
 *
 * \param cfg           The configuration
 * \param rng           The random number generator
 * \param pmaps         Property maps that *may* be used by the graph
 *                      creation algorithms. At this point, only the
 *                      `load_from_file` model will make use of this,
 *                      allowing to populate a `weight` property map.
 *
 * \return Graph        The graph
 */
template<typename Graph, typename RNG>
Graph create_graph(const Config& cfg,
                   RNG& rng,
                   boost::dynamic_properties pmaps
                        = {boost::ignore_other_properties})
{
    // Get the graph generating model
    const std::string model = get_as<std::string>("model", cfg);

    if (model == "ErdosRenyi_GNP") {
        // Get the model-specific configuration options
        const auto& cfg_ER = get_as<Config>("ErdosRenyi_GNP", cfg);
        const auto self_edges = get_as<bool>("self_edges", cfg_ER);
        const auto p = get_as<double>("p", cfg_ER);

        // Decide whether to use the fast algorithm
        if (get_as<bool>("use_fast_algorithm_if_sparse", cfg_ER, false) and
            p < get_as<double>("p_thresh_fast", cfg_ER, 0.1))
        {
            return Creators::create_gnp_random_graph_fast<Graph>(
                get_as<std::size_t>("num_vertices", cfg),
                p,
                self_edges,
                rng
            );
        }
        else {
            return Creators::create_gnp_random_graph<Graph>(
                get_as<std::size_t>("num_vertices", cfg),
                p,
                self_edges,
                rng
            );
        }

    }
    else if (model == "lattice") {
        const auto num_vertices = get_as<std::size_t>("num_vertices", cfg);

        const std::size_t num_cols = sqrt(num_vertices);
        const std::size_t num_rows = num_cols;

        if (num_rows * num_cols != num_vertices) {
            throw std::invalid_argument(
                "Can currently only create lattice graphs that have the "
                "same number of rows and columns. Thus, the `num_vertices` "
                "parameter needs to be the square of an integer, but it was "
                + std::to_string(num_vertices) + "."
            );
        }

        const auto& cfg_lattice = get_as<Config>("lattice", cfg);
        const auto self_edges = get_as<bool>("self_edges", cfg_lattice);
        const auto periodic = get_as<bool>("periodic", cfg_lattice);
        const auto num_neighbors = get_as<int>("num_neighbors", cfg_lattice);

        return Creators::create_lattice_graph<Graph>(
            num_cols,
            num_rows,
            self_edges,
            periodic,
            num_neighbors
        );
    }
    else if (model == "grid") {
        const auto num_vertices = get_as<std::size_t>("num_vertices", cfg, 0);
        const auto mean_degree = get_as<double>("mean_degree", cfg, -1.);
        const auto& cfg_grid = get_as<Config>("grid", cfg);
        const auto self_edges = get_as<bool>("self_edges", cfg_grid);

        return Creators::create_grid_graph<Graph>(
            num_vertices,
            mean_degree,
            cfg_grid,
            self_edges
        );
    }
    else if (model == "WattsStrogatz_grid") {
        const auto num_vertices = get_as<std::size_t>("num_vertices", cfg, 0);
        const auto mean_degree = get_as<double>("mean_degree", cfg);

        const auto& cfg_WSG = get_as<Config>("WattsStrogatz_grid", cfg);
        const auto p_rewire = get_as<double>("p_rewire", cfg_WSG);
        const auto cut_down_to_mean_degree =
            get_as<bool>("cut_down_to_mean_degree", cfg_WSG);
        const auto cut_locally = get_as<bool>("cut_locally", cfg_WSG);
        const auto rewire_fraction = get_as<bool>("rewire_fraction", cfg_WSG);
        const auto self_edges = get_as<bool>("self_edges", cfg_WSG);
        const auto max_trials = get_as<std::size_t>("max_trials", cfg_WSG);

        return Creators::create_WattsStrogatz_grid_graph<Graph>(
            num_vertices,
            mean_degree,
            p_rewire,
            cfg_WSG,
            rng,
            cut_down_to_mean_degree,
            cut_locally,
            rewire_fraction,
            self_edges,
            max_trials
        );
    }
    else {
        try {
            return Utopia::Graph::create_graph<Graph>(cfg, rng, pmaps);
        }
        catch (std::exception& exc) {
            // Adapt error message
            throw std::invalid_argument(
                std::string(exc.what()) +
                "... or any of the SocialSpinModel extensions:  "
                "'ErdosRenyi_GNP', 'lattice', grid', 'WattsStrogatz_grid'"
            );
        }
    }
}

} // namespace Utopia::Utils::Graphs

#endif // UTOPIA_MODELS_SSM_GRAPH_CREATION_HH
