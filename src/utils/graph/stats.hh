#ifndef UTOPIA_SSM_UTILS_GRAPH_STATS_HH
#define UTOPIA_SSM_UTILS_GRAPH_STATS_HH

#include <memory>
#include <vector>

#include <boost/graph/adjacency_list.hpp>

#include <utopia/core/graph/iterator.hh>
#include <utopia/data_io/hdfdataset.hh>
#include <utopia/data_io/hdfgroup.hh>


namespace Utopia::Utils {

/// Computes the (out-)degree distribution for the given graph
/** TODO Generalize to other in-/out-/combined degree
 */
template<class G, class Container = std::vector<uint>>
Container compute_degree_distribution (const G& g) {
    using Utopia::range;
    using Utopia::IterateOver;

    auto dist = Container{};
    auto k = 0u;

    for (const auto& v : range<IterateOver::vertices>(g)) {
        k = boost::out_degree(v, g);

        if (dist.size() <= k) {
            // Need to add at least one zero in the back
            dist.insert(dist.end(), 1 + k - dist.size(), 0);
        }
        dist[k] += 1;
    }

    return dist;
}

/// Coarsens an integer-stepped degree distribution
// TODO Implement
// template<class Container = std::vector<uint>, class T = int>
// std::tuple<Container, uint> coarsen_degree_distribution(
//     const Container& dd,
//     const T num_bins
// ) {


//     return {{}, 0};
// }



namespace IO {
    /// Writes basic stats about a graph to a data group
    template<class G>
    void save_graph_stats (
        const G& g,
        std::shared_ptr<DataIO::HDFGroup> grp,
        const std::string& attr_prefix = ""
    ){
        const auto num_vertices = boost::num_vertices(g);
        const auto num_edges = boost::num_edges(g);

        grp->add_attribute(
            attr_prefix + "num_vertices", num_vertices
        );
        grp->add_attribute(
            attr_prefix + "num_edges", num_edges
        );
        grp->add_attribute(
            attr_prefix + "is_directed", boost::is_directed(g)
        );
        grp->add_attribute(
            attr_prefix + "mean_degree",
            static_cast<double>(num_edges) / num_vertices
        );
    }

    /// Writes a given degree distribution to a new dataset in a given group
    template<class Container>
    auto save_degree_distribution (
        Container&& degree_distribution,
        std::shared_ptr<DataIO::HDFGroup> grp,
        const std::string& dset_name = "degree_distribution"
    ){
        auto dset = grp->open_dataset(dset_name);

        dset->add_attribute("dim_name__0", "k");
        dset->add_attribute("coords_mode__k", "trivial");

        dset->write(degree_distribution);

        return dset;
    }
}

} // namespace

#endif // UTOPIA_SSM_UTILS_GRAPH_STATS_HH
