#ifndef UTOPIA_SSM_UTILS_ITERATOR_HH
#define UTOPIA_SSM_UTILS_ITERATOR_HH

#include <algorithm>
#include <cmath>
#include <iterator>

namespace Utopia::Utils {

/// Returns a begin-iterator that starts `n` elements from the back
/** If n is larger than container size, will be equivalent to the begin
 *  iterator.
 */
template<class Container>
auto from_the_back (const Container& c, const std::size_t n) {
    const auto n_adv = std::clamp<std::size_t>(c.size() - n, 0u, c.size());
    auto it = std::begin(c);
    std::advance(it, n_adv);
    return it;
};

// TODO non-const iterator?

} // namespace Utopia::Utils

#endif // UTOPIA_SSM_UTILS_ITERATOR_HH
